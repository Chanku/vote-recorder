Vote Archive
============

This project is simply a script/bot that automagically updates and maintains a spreadsheet of Underhusen votes. 

This entire project was written for Devember

### Installation  
[TO BE WRITTEN]

### Requirements  
[TO BE WRITTEN]

### Usage  
[TO BE WRITTEN]


Q&A
---

### Why Python 3?  
For this project, it was probably the best option

### What is an Underhusen?  
The lower house of a community on a political simulator

### Why write this?  
Well, originally there was a "better UH Archive" that was supposed to be maintained by the Speaker, however the archive was done in spreadsheet, which required the previous Speaker to transfer it to the new speaker, which lead to it eventually being forgotten about and falling behind.

This is a revision of that idea, but using programming to automate it


Copyright
---------
All code in this repository belongs to Chanku, Copyright 2017, and is licensed under the MIT (Expat) License, a copy of which is found in the LICENSE file
