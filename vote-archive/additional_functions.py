""" Just some additional functions. """
import re
import winterscrape.winterscrape

def _check_vote(text):
    is_vote = ('aye' in text) or ('nay' in text) or ('abstain' in text)
    return is_vote

def _get_vote(text):
    if 'aye' in text:
        return 'Aye'
    elif 'nay' in text:
        return 'Nay'
    elif 'abstain' in text:
        return 'Abstain'

def _check_not_voting(text):
    not_voting = ('Not Voting' in text)
    return text

def _check_if_results(text):
    is_results = ('aye' in text) and ('nay' in text) and ('abstain' in text) and ('not voting' in text)
    return is_results

def _check_if_vote(subject):
    is_vote = subject.startswith('closed') or subject.startswith('[passed')
    is_vote = is_vote or subject.startswith('[tied]') or subject.startswith('[failed')
    is_vote = is_vote or subject.startswith('[vote') or subject.startswith('[defeat')
    is_vote = is_vote or subject.startswith('[defeated') or subject.startswith('vote')
    is_vote = is_vote or subject.startswith('[defeat]') or subject.startswith('[tie')
    is_vote = is_vote or subject.startswith('(vote') or subject.startswith('tie')
    is_vote = is_vote or subject.startswith('passed') or re.match('\[[0-9].*', subject)
    is_vote = is_vote or subject.startswith('(passed') or subject.startswith('(tie')
    is_vote = is_vote or subject.endswith('passed)') or subject.endswith('vote]')
    return is_vote

def perform_vote(posts, underhusen_members):
    """ Get the votes and return the vote dict """
    vote_dictionary = {}
    results_post = {}
    for post in posts:
        if _check_if_results(post['post_text'].lower()):
            results_post = post
        elif _check_vote(post['post_text'].lower()):
            author = post['poster']
            vote_dictionary[author] = post['post_text']

    unknown_members = set(vote_dictionary) ^ set(underhusen_members)
    not_voting_count = 0
    for unknown in unknown_members:
        vote_dictionary[unknown] = 'Not Voting'
        not_voting_count += 1

    speaker_not_voting_count = results_post['post_text'].lower().split("Not Voting:")[1].split(' ')[0]
    try:
        speaker_not_voting_count = int(speaker_not_voting_count)
    except ValueError:
        print("Please enter the correct value of the Not Voting Count, or press [ENTER] to raise error")
        print("Speaker Post: {}".format(results_post['post_text']))
        correction = input("> ")
        if not correction:
            raise
        speaker_not_voting_count = correction
    
    if speaker_not_voting_count != not_voting_count:
        print("Speaker Not Voting Count is different than the calculated not voting count")
        print("Speaker Count: {}".format(speaker_not_voting_count))
        print("Calculated Not Voting Count: {}".format(not_voting_count))
        print("Voting Dictionary: {}".format(voting_dictionary))
        while True:
            print("Please Provide one of the following resolutions: [r]emove Not Voting, [c]orrect records, [e]rror")
            action = input("> ")
            action = action.lower()
            if action.startswith('r'):
                raise NotImplementedError
                break
            elif action.startswith('c'):
                raise NotImplementedError
                break
            elif action.startswith('e')
                raise ValueError("Error with calculation")
                break
    return voting_dictionary

def scrape_underhusen_archive():

    #TODO: Change get_subforum and get_subforum_page to return dicts
    archive_data = winterscrape.winterscrape.get_subforum('Underhusen Archive', 'The Registry of Things Past')
    
    ### NOTE: Remove this code when the API is fully implemented ###
    pages = archive_data.find('div', class_="pagesection")
    last_page = archive_data.find_all('a', class_='navPages')[-1]
    last_page = last_page.text

    last_page_data = winterscrape.winterscrape.get_subforum_page('Underhusen Archive', last_page)

    ### NOTE: Remove this code when the API is fully implemented ###
    topic_elements = last_page_data.find('div', id="messageindex")
    topic_elements = topic_elements.find('tbody').find_all('tr')[0:-1]
    ################################################################
    ### NOTE: Rewrite this code when the API is fully implemented ###
    topics = []
    for tag in topic_elements:
        subject = tag.find('td', class_=re.compile('subject'), recursive=True)
        subject_text = subject.find('a').text
        is_vote = _check_if_vote(subject_text.lower())
        if is_vote:
            topics.append(subject_text)

    add_new_skrifa('Crya', '24')
    vote_dict = {('24', 'Crya'):'Not Voting'}
    for topic in topics:
        topic_data = winterscrape.winterscrape.get_all_posts_from_topic(topic, data=last_page_data)
        posts = topic_data['posts'][1:-1]
        final_post = topic_data['posts'][-1]
        for post in posts:
            if _check_vote(post['post_text']):
                poster = post['poster']
                vote_dict[poster] = _get_vote(post.lower())
                add_new_skrifa(*poster)
        add_new_bill(topic_data['thread_name'], vote_dict)
    raise NotImplementedError
    ################################################################




def add_new_skrifa(username, user_id):
    """ Adds a new skrifa to the Spreadsheet.

        :param username: Username of the Skrifa
        :param user_id: The User ID of the Skrifa
        :type: username: str
        :type: user_id: int
    """
    raise NotImplementedError

def update_skrifa_username(new_username, user_id):
    """ Update's the username of an existing skrifa on the Spreadsheet.

        :param new_username: Username of the Skrifa
        :param user_id: The User ID of the Skrifa
        :type: new_username: str
        :type: user_id: int
    """
    raise NotImplementedError

def check_for_bill(bill_title):
    """ Checks for the existence of a bill in the archive.

        :param bill_title: The title of the bill
        :type bill_title: str

        :returns: Bool (True if in the spreadsheet, False if not)
    """
    raise NotImplementedError

def update_bill(bill_title, vote_dict, check_outcome=True):
    """ Updates an existing bill in the archive.
        
        :param bill_title: The title of the bill
        :param vote_dict: A dictionary of all Skrifa Votes.
        :param check_outcome: Whether or not to recheck the outcome of a bill, default is true
        :type bill_title: str
        :type vote_dict: dict
        :type check_outcome: bool
    """
    raise NotImplementedError

def add_new_bill(bill_title, vote_dict, outcome=None):
    """ Adds a new bill to the archive
        
        :param bill_title: The title of the bill.
        :param vote_dict: The dictonary of skrifa votes.
        :param outcome: The outcome of the vote, if None it automagically calculates it.
        :type bill_title: str
        :type vote_dict: dict
        :type outcome: bool
    """
    raise NotImplementedError

def calculate_bill_outcome(bill_title, vote_dict=None):
    """ Calculates the outcome of the bill.
        
        :param bill_title: The title of the bill
        :param vote_dict: All of the votes by all skrifa, if None it uses the archive.
        :type bill_title: str
        :type vote_dict: dict
    """
    raise NotImplementedError
