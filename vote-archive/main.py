""" This is the main python file, which is where the bot itself runs. """
import additional_functions
import winterscrape.winterscrape
import gspread
import oauth2client.service_account as oauth

SHEET_NAME = 'Devtember'

def first_run(sheet_client):
    """ Sets up the Spreadsheet for the first run 

        :param sheet_client: This is the sheet_client from init()
        :type sheet_client: gspread.Spreadsheet
    """
    additional_functions.scrape_underhusen_archive()

    
def init():
    """ Init the Modules """
    scope = ['https://spreadsheets.google.com/feeds']
    
    credentials = oauth.ServiceAccountCredentials.from_json_keyfile_name('creds.json', scope)
    sheet_client = gspread.authorize(credentials)

    winterscrape.winterscrape.setup_module(True, False, user_agent="Vote-Archiver")

    return sheet_client

def main():
    """The main function"""

    sheet_client = init()
    worksheet_1 = sheet_client.open(SHEET_NAME).sheet1
    worksheet_1.update_cell(1, 1, 'New Val2!')

    if worksheet_1.acell('B1').value:
        raise NotImplementedError("Section of Code Not Implemented!")
    else:
        first_run(sheet_client)


if __name__ == "__main__":
    main()
